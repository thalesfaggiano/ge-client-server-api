package main

import (
	"os"
	"fmt"
	"log"
	"time"
	"context"
	"net/http"
	"encoding/json"
)

const (
	servidor = "http://localhost:8080/cotacao"
	expiracao   = 300 * time.Millisecond
)

type Cotacao struct {
	Bid string `json:"bid"`
}

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), expiracao)
	defer cancel()
	cotacao, err := pegaCotacao(ctx)
	if err != nil {
		log.Fatalf("Erro main, pegaCotacao: %v", err)
	}
	if err := salvaArquivo(cotacao); err != nil {
		log.Fatalf("Erro main, salvaArquivo: %v", err)
	}
	fmt.Println("Cotação salva: cotacao.txt")
}

func pegaCotacao(ctx context.Context) (*Cotacao, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", servidor, nil)
	if err != nil {
		return nil, fmt.Errorf("Erro pegaCotacao, NewRequest: %v", err)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Erro pegaCotacao, client.Do: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Erro pegaCotacao, StatusCode: %v", resp.Status)
	}
	var cotacao Cotacao
	if err := json.NewDecoder(resp.Body).Decode(&cotacao); err != nil {
		return nil, fmt.Errorf("Erro pegaCotacao, Decoder: %v", err)
	}
	return &cotacao, nil
}

func salvaArquivo(cotacao *Cotacao) error {
	content := fmt.Sprintf("Dólar: %s", cotacao.Bid)
	if err := os.WriteFile("cotacao.txt", []byte(content), 0644); err != nil {
		return fmt.Errorf("Erro salvaArquivo, WriteFile: %v", err)
	}
	return nil
}
