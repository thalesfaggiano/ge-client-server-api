package main

import (
	"fmt"
	"log"
	"time"
	"context"
	"net/http"
	"gorm.io/gorm"
	"encoding/json"
	"gorm.io/driver/sqlite"
)

type respostaAPI struct {
	USDBRL Cotacao `json:"USDBRL"`
}

type Cotacao struct {
	gorm.Model
	Oferta string `json:"bid"`
}

const (
	porta = ":8080"
	tempoAPI = 200 * time.Millisecond
	tempoDB  = 10 * time.Millisecond
	api     = "https://economia.awesomeapi.com.br/json/last/USD-BRL"
)

func iniciaDB() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("cotacoes.db"), &gorm.Config{})
	if err != nil {
		log.Fatalf("Erro iniciaDB, Open: %v", err)
	}
	err = db.AutoMigrate(&Cotacao{})
	if err != nil {
		log.Fatalf("Erro iniciaDB, AutoMigrate: %v", err)
	}
	return db
}

func manipulaCotacao(db *gorm.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), tempoAPI)
		defer cancel()
		respostaAPI, err := pegaCotacao(ctx)
		if err != nil {
			http.Error(w, fmt.Sprintf("Erro manipulaCotacao, pegaCotacao: %v", err), http.StatusInternalServerError)
			return
		}
		dbCtx, dbCancel := context.WithTimeout(context.Background(), tempoDB)
		defer dbCancel()

		if err := salvaCotacao(dbCtx, db, respostaAPI.USDBRL.Oferta); err != nil {
			http.Error(w, fmt.Sprintf("Erro manipulaCotacao, salvaCotacao: %v", err), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(respostaAPI.USDBRL)
	}
}

func pegaCotacao(ctx context.Context) (*respostaAPI, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", api, nil)
	if err != nil {
		return nil, fmt.Errorf("Erro pegaCotacao, NewRequest: %v", err)
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Erro pegaCotacao, client.Do: %v", err)
	}
	defer resp.Body.Close()
	var respostaAPI respostaAPI 
	if err := json.NewDecoder(resp.Body).Decode(&respostaAPI); err != nil {
		return nil, fmt.Errorf("Erro pegaCotacao, NewDecoder: %v", err)
	}
	return &respostaAPI, nil
}

func salvaCotacao(ctx context.Context, db *gorm.DB, bid string) error {
	cotacao := Cotacao{
		Oferta: bid,
	}
	result := db.WithContext(ctx).Create(&cotacao)
	if result.Error != nil {
		return fmt.Errorf("Erro salvaCotacao, result.Error: %v", result.Error)
	}
	return nil
}

func main() {
	db := iniciaDB()
	http.HandleFunc("/cotacao", manipulaCotacao(db))
	log.Printf("Porta %s", porta)
	log.Fatal(http.ListenAndServe(porta, nil))
}
